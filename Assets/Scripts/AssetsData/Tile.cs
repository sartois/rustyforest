﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Tile")]
public class Tile : ScriptableObject
{
    public enum Direction
    {
        North, East, South, West
    }

    public Vector2 size = new Vector2(100, 100);

    public Direction entryDirection = Direction.South;
    public Direction exitDirection = Direction.North;

    public GameObject tileTerrain;

    public int pathStartX = 40;
    public int pathWidth = 40;
}
