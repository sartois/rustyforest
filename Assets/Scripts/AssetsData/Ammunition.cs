﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Ammunition")]
public class Ammunition : ScriptableObject
{
    public GameObject avatar;
    public int dammage;
}
