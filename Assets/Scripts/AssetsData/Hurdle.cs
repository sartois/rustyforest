﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Hurdle")]
public class Hurdle : ScriptableObject
{
    public GameObject avatar;
    [Range(0, 1)]
    public float difficulty;

    public float scaleMin = .8f;
    public float scaleMax = 1.5f;

    public bool addForce = true;
}
