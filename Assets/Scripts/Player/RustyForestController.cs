﻿using UnityEngine;

/**
 * Keyboard only "physics" based controller
 */
public class RustyForestController : AController
{
    //Degres par seconde par valeur d input.
    public float XSensivity = 360;
    public float scalerMin = 0.5f;
    public float scalerMax = 1.2f;

    void Update()
    {
        DrawDebug();

        UpdateCursorLock();

        GetInput();
    }

    void GetInput()
    {
        float rotation = Input.GetAxis("Horizontal");
        float rawAccelerationScaler = Input.GetAxis("Vertical");
        wantJump = Input.GetButtonDown("Jump");
        accelerationScaler = Utils.map(rawAccelerationScaler, -1, 1, scalerMin, scalerMax);

        if (rotation != 0)
        {
            rotation = rotation *= -0.6f;
            Quaternion rotateHorizontal = Quaternion.AngleAxis(rotation * XSensivity * Time.deltaTime, Vector3.up);
            wantedDirectionTargetSmooth = rotateHorizontal * wantedDirectionTargetSmooth;
            wantedDirection = smoothWantedDirection(Time.deltaTime);
        }
    }
}
