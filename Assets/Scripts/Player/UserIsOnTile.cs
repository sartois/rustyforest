﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserIsOnTile : MonoBehaviour
{
    static public int tileNb { get; protected set; } = 1;

    void Update()
    {
        tileNb = Mathf.CeilToInt(transform.position.z / SharedConst.TILE_DEPTH);
    }
}
