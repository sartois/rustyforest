﻿using UnityEngine;

[RequireComponent(typeof(RustyForestCharacter))]
public class PineconeBazooka : MonoBehaviour
{
    public Transform ammunition;
    public float ammunitionStartSpeed = 10;
    public float offsetForwardShoot = 2;
    public float timeBetweenShots = 0.5f;

    private RustyForestCharacter character;
    private float timeShoot = 0;
    private bool wantsToShoot = false;

    void Start()
    {
        character = GetComponent<RustyForestCharacter>();
    }

    void Update()
    {
        //@todo should we handle that in the RustyForestController ?
        wantsToShoot = Input.GetKeyDown(KeyCode.W);
    }

    private void FixedUpdate ()
    {
        if (ammunition == null)
        {
            Debug.Log("No ammunition");
            return;
        }

        timeShoot -= Time.deltaTime;

        if (wantsToShoot && timeShoot <= 0 && character.ammunitionCount > 0)
        {
            timeShoot = timeBetweenShots;

            Transform proj = GameObject.Instantiate<Transform>(ammunition,
                transform.position + Vector3.up/2 - (transform.forward * offsetForwardShoot), transform.rotation);

            proj.parent = transform;

            //Ajout d une impulsion de départ
            proj.GetComponent<Rigidbody>().AddForce(-transform.forward * ammunitionStartSpeed, ForceMode.Impulse);

            character.ammunitionCount--;
        }
    }
}
