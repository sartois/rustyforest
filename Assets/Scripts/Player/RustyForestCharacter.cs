﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(AController))]
public class RustyForestCharacter : MonoBehaviour
{    
    public float MaxSpeed = 10;
    public float rotationSpeed = 180;
    public float groundTouchDistance = 1f;
    public float PowerJump = 10;
    public bool DisableDebug = true;
    public int ammunitionCount = 10;
    public int maxHealth = 100;

    public int health { get; protected set; }

    private const string JumpAnimationName = "Jump In Place";
    private const string JumpAnimationTrigger = "isJumping";
    private const string IsDeadTrigger = "isDead";
    
    private Rigidbody _rigidbody;
    private Collider _collider;
    private Animator _animator;
    private AController _controller;

    private Vector3 nextVelocity;
    private Vector3 centerY;
    Vector3 dirToGround;
    private RaycastHit hitInfo;
    private bool _dead = false;
    private bool isJumping = false;
   
    private void Start()
    {
        _collider = GetComponent<Collider>();
        _rigidbody = GetComponent<Rigidbody>();
        _animator = GetComponentInChildren<Animator>();
        _controller = GetComponent<AController>();

        centerY = new Vector3(0, _collider.bounds.size.y / 2f, 0);

        health = maxHealth;
    }

    private void FixedUpdate()
    {
        move(_controller.wantedDirection, _controller.accelerationScaler, _controller.wantJump);
    }

    public void isHit()
    {
        
    }

    public void isDead()
    {
        if (!_dead)
        {
            _dead = true;
            _rigidbody.isKinematic = true;
            _animator.SetTrigger(IsDeadTrigger);

            StartCoroutine(BackToMenu());
        }
    }

    public void dammage(int dammageCount)
    {
        health -= dammageCount;
    }

    private void move(Vector3 wantedDirection, float accelerationScaler, bool jump)
    {
        if (!_dead)
        {
            float distToGround;
            dirToGround =  Vector3.zero;
            isJumping = _animator.GetCurrentAnimatorStateInfo(0).IsName(JumpAnimationName);

            if (Physics.Raycast(transform.position + centerY, Vector3.down, out hitInfo, 5f))
            {
                distToGround = transform.position.y - hitInfo.point.y;
                if (distToGround>0.5 && !isJumping) distToGround *= 2;
                dirToGround = new Vector3(0, -distToGround, 0);
            }

            wantedDirection.y = 0;
            nextVelocity = wantedDirection * MaxSpeed * accelerationScaler;
            nextVelocity = Vector3.ClampMagnitude(nextVelocity, 250);

            //Disallow double jump
            if (jump && isJumping)
            {
                jump = false;
            }

            if (jump || isJumping)
            {
                nextVelocity.y = _rigidbody.velocity.y;
                _rigidbody.velocity = nextVelocity;
                _rigidbody.MoveRotation(Quaternion.LookRotation(wantedDirection, Vector3.up));
                if (jump) {
                    Vector3 v = nextVelocity.normalized;
                    Vector3 jumpForce = new Vector3(v.x, 1, v.z) * PowerJump;
                    _rigidbody.AddForce(jumpForce, ForceMode.Impulse);
                }
            } else
            {
                nextVelocity += dirToGround;
                _rigidbody.velocity = nextVelocity;
                _rigidbody.MoveRotation(Quaternion.LookRotation(wantedDirection, Vector3.up));
            }

            updateAnimator(jump);
        }
    }

    private void updateAnimator(bool jump)
    {
        if (jump)
        {
            _animator.SetTrigger(JumpAnimationTrigger);
        }
    }

    IEnumerator BackToMenu()
    {
        yield return new WaitForSeconds(3f);
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
}
