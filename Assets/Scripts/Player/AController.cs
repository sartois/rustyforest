﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AController : MonoBehaviour
{
    public float smoothFactor = 0.2f;
    public bool lockCursor = true;

    public Vector3 wantedDirection { get; protected set; } = new Vector3(0, 0, 1);
    protected Vector3 wantedDirectionTargetSmooth = new Vector3(0, 0, 1);

    public float wantedSpeed { get; protected set; } = 0;
    public float accelerationScaler { get; protected set; } = 1;

    public bool wantJump { get; protected set; } = false;

    private const float smoothFactorDivisor = 1e6f;
    private bool hasFocus = false;

    protected Vector3 smoothWantedDirection(float deltaTime)
    {
        float t = Mathf.Clamp(1 - Mathf.Pow(smoothFactor / smoothFactorDivisor, deltaTime), 0, 1);
        return Vector3.Lerp(wantedDirection, wantedDirectionTargetSmooth, t).normalized;
    }

    protected void DrawDebug()
    {
#if UNITY_EDITOR
        Debug.DrawLine(transform.position, transform.position + wantedDirection * 2.0f, Color.blue);
        Debug.DrawLine(transform.position, transform.position + wantedDirectionTargetSmooth * 2.0f, Color.red);
#endif
    }

    protected void UpdateCursorLock()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            hasFocus = false;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            hasFocus = true;
        }

        if (hasFocus && lockCursor)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else if (!hasFocus)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
}
