﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RustyForestCharacter))]
public class PlayerAmmunitionCollector : MonoBehaviour
{ 
    private RustyForestCharacter character;
    
    void Start()
    {
        character = GetComponent<RustyForestCharacter>();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == SharedConst.TAG_PINECONE)
        {
            collision.gameObject.SetActive(false);
            character.ammunitionCount++;
        }
    }
}
