﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RustyForestCharacter))]
public class PlayerDammage : MonoBehaviour
{
    public GameObject dammageAnimation;
    private GameObject animationInstance;
    private RustyForestCharacter character;

    void Start()
    {
        character = GetComponent<RustyForestCharacter>();
    }

    void OnCollisionEnter(Collision collision)
    {
        string tag = collision.gameObject.tag;
        AVillainController villainController = collision.gameObject.GetComponent<AVillainController>();

        // @todo use layer ?
        if (tag == SharedConst.TAG_VILLAIN || tag == SharedConst.TAG_AMMUNITION)
        { 
            if (tag == SharedConst.TAG_AMMUNITION)
            {
                Vector3 velocity = collision.gameObject.GetComponent<Rigidbody>().velocity;
                //no dammage for almost static ammunition
                if (velocity.magnitude <= 2f) return;
            }

            //@todo hardcoded dammage ammunition
            int dammage = villainController != null ? villainController.villain.directDammage : 5;
            character.dammage(dammage);

            animationInstance = Instantiate(dammageAnimation, transform.position, Quaternion.identity);
            animationInstance.transform.parent = transform;

            StartCoroutine(WaitAndDisableAnimation(animationInstance));

            if (character.health <= 0)
            {
                character.isDead();
            }
            else
            {
                character.isHit();
            }
        }
    }

    IEnumerator WaitAndDisableAnimation(GameObject animation)
    {
        yield return new WaitForSeconds(1f);
        animation.SetActive(false);
    }
}
