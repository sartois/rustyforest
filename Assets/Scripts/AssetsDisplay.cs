﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RustyForestCharacter))]
public class AssetsDisplay : MonoBehaviour
{
    public Text healthDisplay;
    public Text pineconeDisplay;
    public Text statusDisplay;

    private RustyForestCharacter character;

    void Start()
    {
        character = GetComponent<RustyForestCharacter>();
    }

    void Update()
    {
        if (healthDisplay != null)
        {
            healthDisplay.text = "Health: " + character.health.ToString();
        }

        if (character != null)
        {
            pineconeDisplay.text = "Pinecone: " + character.ammunitionCount.ToString();
        }

        if (statusDisplay != null)
        {
            statusDisplay.text = getStatusFromTileCount(UserIsOnTile.tileNb);
        }
    }

    private string getStatusFromTileCount(int count)
    {
        switch(count)
        {
            case 1:
            case 2:
                return "Wannabe Jogger";
            case 3:
                return "Newbie";
            case 4:
                return "Casual Jogger";
            case 5:
                return "Sunday Jogger";
            case 6:
                return "Not so bad Jogger";
            case 7:
                return "Real Jogger";
            case 8:
                return "Rocky Balboa";
            case 9:
            default:
                return "Kenyan runner";

        }
    }
}
