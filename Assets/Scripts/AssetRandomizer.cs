﻿using UnityEngine;

public class AssetRandomizer
{
    static public Vector3 scale(Hurdle hurdle, float scaler = 1)
    {
        float randFactor = Random.Range(hurdle.scaleMin, hurdle.scaleMax);
        Vector3 test = new Vector3(randFactor, randFactor, randFactor) * scaler;
        return test;
    }

    static public Vector3 force(float scaler = 1)
    {
        Vector3 test = new Vector3(Random.Range(0f, 1f), -1, Random.Range(0f, 1f));
        test *= scaler;
        return test;
    }
}
