﻿using UnityEngine;

public class CollisionInfoDisplayer : MonoBehaviour
{
    public delegate void DisplayInfoCollision(string name);
    public static event DisplayInfoCollision DisplayInfoNeeded;

    private bool usedOnce = false;

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (!usedOnce)
            {
                usedOnce = true;
                DisplayInfoNeeded(name);
            }
        }
    }
}
