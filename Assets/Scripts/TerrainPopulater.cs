﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainPopulater : MonoBehaviour
{
    public Hurdle[] natureItems;
    public Hurdle[] trashes;
    public GameObject ammunition;
    public Tile tile;
    public const int STEP = 5;
    public bool testDifficulties = false;

    private GameObject attachedTerrain;
    private Transform attachedTransform;

    private RaycastHit hit;

    //Let the user see pincecones and hurdles falling when starting because I like it
    private void Start()
    {
        if (testDifficulties)
        {
            testDifficultiesMethod();
        }

        //The tile is straight
        if (tile.exitDirection == Tile.Direction.North && tile.entryDirection == Tile.Direction.South ||
            tile.entryDirection == Tile.Direction.East && tile.exitDirection == Tile.Direction.West ||
            tile.entryDirection == Tile.Direction.West && tile.exitDirection == Tile.Direction.East)
        {
            float difficulty = Utils.getCroissantDifficulty(UserIsOnTile.tileNb * 1f);
            float inverseDifficulty = Utils.getDecroissantDifficulty(UserIsOnTile.tileNb * 1f);
            float delay = UserIsOnTile.tileNb == 1 ? 0f : 0.5f;

            StartCoroutine(addStuffs(delay, new List<Hurdle>(natureItems), difficulty, true));
            StartCoroutine(addNonPhysicsStuff(UserIsOnTile.tileNb == 1 ? 0f : 0.1f, new List<Hurdle>(trashes), difficulty, true, Mathf.RoundToInt(STEP/2 * inverseDifficulty)));

            //Add ammunition after hurdles so that they can fall on hurdles
            StartCoroutine(addAmmunitions(delay, difficulty));

        } else
        {
            //The tile is curve @todo
        }
    }

    private IEnumerator addAmmunitions(float delay, float difficulty)
    {
        WaitForSeconds wait = new WaitForSeconds(delay);
        float x = getRandX();

        for (int z = 1; z < tile.size.y; z += STEP)
        {
            //move the populater
            transform.localPosition = new Vector3(x, transform.localPosition.y, z);

            if (Random.value < difficulty)
            {
                if (Physics.Raycast(transform.position, Vector3.down, out hit))
                {
                    //Give it some height to let a chance to fall
                    GameObject pinecone = Instantiate(ammunition, hit.point + Vector3.up * 2, Quaternion.identity);
                    pinecone.transform.parent = hit.transform;

                } else {
                    Debug.Log("No collision at ("+x+","+z+")");
                }
            } else
            {
                x = getRandX();
            }

            yield return wait;
        }
    }

    private IEnumerator addStuffs(float delay, List<Hurdle> stuff, float difficulty, bool mayScale = false, int step = STEP)
    {
        WaitForSeconds wait = new WaitForSeconds(delay);
        List<Hurdle> filteredStuff = new List<Hurdle>();

        foreach (Hurdle hurdle in stuff)
        {
            if (hurdle.difficulty < difficulty) filteredStuff.Add(hurdle);
        }

        for (int z = step; z < tile.size.y; z += step)
        {
            if (Random.value < difficulty)
            {
                float x = getRandX();
                transform.localPosition = new Vector3(x, transform.localPosition.y, z);
                Hurdle h = filteredStuff[Random.Range(0, filteredStuff.Count)];

                addItem(h, mayScale, x, z);
            }

            yield return wait;
        }
    }

    private IEnumerator addNonPhysicsStuff(float delay, List<Hurdle> stuff, float difficulty, bool mayScale = false, int step = STEP)
    {
        WaitForSeconds wait = new WaitForSeconds(delay);
        List<Hurdle> filteredStuff = new List<Hurdle>();

        foreach (Hurdle hurdle in stuff)
        {
            if (hurdle.difficulty < difficulty) filteredStuff.Add(hurdle);
        }

        for (int z = step; z < tile.size.y; z += step)
        {
            if (Random.value < difficulty)
            {
                float x = getRandX();
                transform.localPosition = new Vector3(x, transform.localPosition.y, z);
                Hurdle h = filteredStuff[Random.Range(0, filteredStuff.Count)];

                if (Physics.Raycast(transform.position, Vector3.down, out hit, 51f))
                {
                    if (hit.transform.gameObject.name.StartsWith("Terrain"))
                    {
                        GameObject curTerrain = hit.transform.gameObject;
                        float scaler = TilesInterlockController.tilesAdded > 2 ? Mathf.Log(TilesInterlockController.tilesAdded) : 1;

                        GameObject go = Instantiate(h.avatar, hit.point, Quaternion.identity);
                        go.transform.parent = curTerrain.transform;

                        if (mayScale)
                        {
                            go.transform.localScale = AssetRandomizer.scale(h, scaler);
                            go.transform.rotation = Random.rotation;
                        }
                    }
                }
            }

            yield return wait;
        }
    }

    private void addItem(Hurdle h, bool mayScale, float x, int z)
    {
        if (Physics.Raycast(transform.position, Vector3.down, out hit))
        {
            if (hit.transform.gameObject.name.StartsWith("Terrain"))
            {
                GameObject curTerrain = hit.transform.gameObject;
                float scaler = TilesInterlockController.tilesAdded > 2 ? Mathf.Log(TilesInterlockController.tilesAdded) : 1;

                GameObject go = Instantiate(h.avatar, hit.point + Vector3.up * 2, Quaternion.identity);
                go.transform.parent = curTerrain.transform;

                if (mayScale)
                {
                    go.transform.localScale = AssetRandomizer.scale(h, scaler);
                }

                if (h.addForce)
                {
                    go.GetComponent<Rigidbody>().AddTorque(AssetRandomizer.force(scaler * 500), ForceMode.Impulse);
                }
            }
        }
        
    }

    private float getRandX()
    {
        return Random.Range(tile.pathStartX, tile.pathStartX + tile.pathWidth);
    }

    public void testDifficultiesMethod()
    {
        for (int i = 0 ; i < 15; i++ )
        {
            Debug.Log("Test difficulty: getDecroissantDifficulty, i: " + i + ", result: " + Utils.getDecroissantDifficulty(i * 1f));
        }

        for (int i = 0; i < 15; i++)
        {
            Debug.Log("Test difficulty: getCroissantDifficulty, i: " + i + ", result: " + Utils.getCroissantDifficulty(i * 1f));
        }
    }
}
