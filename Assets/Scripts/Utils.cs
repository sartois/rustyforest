﻿using UnityEngine;

public class Utils
{
    static public float getDecroissantDifficulty(float tileCount)
    {
        return (1.0f / (tileCount < 4f ? 4f : tileCount)) * 2;
    }

    static public float getCroissantDifficulty(float tileCount)
    {
        if (tileCount > 25) tileCount = 25;
        float mapped = map(tileCount, 1f, 25f, 0.0625f * (float)Mathf.PI, 0.5f * (float)Mathf.PI);
        return Mathf.Sin(mapped);
    }

    //Something like the map well known Processing function https://processing.org/reference/map_.html
    static public float map(float currentVal, float currentMin, float currentMax, float targetMin, float targetMax)
    {
        return targetMin + (targetMax - targetMin) * ((currentVal - currentMin) / (currentMax - currentMin));
    }

}
