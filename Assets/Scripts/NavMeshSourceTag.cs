﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections.Generic;

// Base source:  https://github.com/Unity-Technologies/NavMeshComponents
// Tagging component for use with the LocalNavMeshBuilder
// Supports mesh-filter and terrain
//
// extended to physics 
[DefaultExecutionOrder(-200)]
public class NavMeshSourceTag : MonoBehaviour
{
    // Global containers for all active mesh/terrain tags
    public static List<Terrain> m_Terrains  = new List<Terrain>();

    public static Dictionary<string, TerrainData> savedTerrainData = new Dictionary<string, TerrainData>();

    //public static Dictionary<string, Terrain> m_Terrains = new Dictionary<string, Terrain>();
    //public static List<MeshFilter> m_Meshes = new List<MeshFilter>();
    //public static List<BoxCollider> m_Colliders = new List<BoxCollider>();

    void Start()
    {
        PopulateLists();
    }

    void OnDisable()
    {
        var t = GetComponent<Terrain>();
        if (t != null)
        {
            m_Terrains.Remove(t);
        }

        /* var m = GetComponent<MeshFilter>();
        if (m != null)
        {
            m_Meshes.Remove(m);
        }

        var b = GetComponent<BoxCollider>();
        if (b != null)
        {
            m_Colliders.Remove(b);
        } */
    }

    void PopulateLists()
    {
        var t = GetComponent<Terrain>();
        if (t != null)
        {
            m_Terrains.Add(t);

            if (!savedTerrainData.ContainsKey(t.name))
            {
                savedTerrainData.Add(t.name, t.terrainData);
            }

            /* if (!m_Terrains.ContainsKey(t.name))
            {
                m_Terrains.Add(t.name, t);
            }
            
            if (!savedNavMeshBuildSource.ContainsKey(t.name))
            {
                savedNavMeshBuildSource.Add(t.name, new List<NavMeshBuildSource>());
            } */
        }

        /* var b = GetComponentsInChildren<BoxCollider>(false);
        if (b != null)
        {
            m_Colliders.AddRange(b);
        } */

        /*var m = GetComponentsInChildren<MeshFilter>();
        if (m != null)
        {
            for(int i= 0;  i<m.Length; i++)
            {
                if (m[i].tag != "Villain" && !m_Meshes.Contains(m[i]))
                {
                    m_Meshes.Add(m[i]);
                }
            }
        }*/
    }

    // Collect all the navmesh build sources for enabled objects tagged by this component
    public static void Collect(ref List<NavMeshBuildSource> sources)
    {
        sources.Clear();

        for (var i = 0; i < m_Terrains.Count; ++i)
        {
            var t = m_Terrains[i];
            if (t == null) continue;

            var s = new NavMeshBuildSource();
            s.shape = NavMeshBuildSourceShape.Terrain;
            s.sourceObject = savedTerrainData[t.name];

            // Terrain system only supports translation - so we pass translation only to back-end
            s.transform = Matrix4x4.TRS(t.transform.position, Quaternion.identity, Vector3.one);
            s.area = 0;
            sources.Add(s);
        }

        //@see https://forum.unity.com/threads/navmeshbuildsource-for-physics-colliders.482270/
        /* for (var i = 0; i < m_Colliders.Count; ++i)
        {
            var c = m_Colliders[i];
            if (c == null) continue;
            if (c.isTrigger) continue;

            var s = new NavMeshBuildSource();
            s.shape = NavMeshBuildSourceShape.Box;
            s.component = c;
            var center = c.transform.TransformPoint(c.center);
            var scale = c.transform.lossyScale;
            var size = new Vector3(c.size.x * Mathf.Abs(scale.x), c.size.y * Mathf.Abs(scale.y), c.size.z * Mathf.Abs(scale.z));
            s.transform = Matrix4x4.TRS(center, c.transform.rotation, Vector3.one);
            s.size = size;
            s.area = 0;
            sources.Add(s);
        }*/

        /* for (var i = 0; i < m_Meshes.Count; ++i)
        {
           var mf = m_Meshes[i];
           if (mf == null) continue;

           var m = mf.sharedMesh;
           if (m == null) continue;

           var s = new NavMeshBuildSource();
           s.shape = NavMeshBuildSourceShape.Mesh;
           s.sourceObject = m;
           s.transform = mf.transform.localToWorldMatrix;
           s.area = 0;
           sources.Add(s);
       } */
    }
}