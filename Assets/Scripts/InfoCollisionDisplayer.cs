﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoCollisionDisplayer : MonoBehaviour
{
    public UnityEngine.UI.Text displayer;

    public float delay = 5f;

    private void OnEnable()
    {
        CollisionInfoDisplayer.DisplayInfoNeeded += DisplayInfo;
    }

    private void OnDisable()
    {
        CollisionInfoDisplayer.DisplayInfoNeeded -= DisplayInfo;
    }

    void DisplayInfo(string name)
    {
        //fragile...
        if (name == "CollisionInfoDisplayer#1")
        {
            displayer.text = "Rotate and speed with arrows";
            StartCoroutine(WaitAndDeactivate());
        }

        if (name == "CollisionInfoDisplayer#2")
        {
            displayer.text = "Collect pinecone, might be usefull... Shoot with w";
            StartCoroutine(WaitAndDeactivate());
        }
    }

    IEnumerator WaitAndDeactivate()
    {
        yield return new WaitForSeconds(delay);
        displayer.text = "";
    }
}
