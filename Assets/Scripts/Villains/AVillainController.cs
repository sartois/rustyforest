﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(NavMeshRealTimeBuilder))]
public abstract class AVillainController : MonoBehaviour
{
    public delegate void VillainDeathHandler(GameObject villain);
    public static event VillainDeathHandler OnDeath;

    public Villain villain;
    public int health;
    static public bool freeze = false; 

    protected GameObject ennemy;
    protected Transform target = null;
    protected RustyForestCharacter ennemyController;
    protected NavMeshRealTimeBuilder nmRealTimeBuilder;
    protected NavMeshAgent navMeshAgent;
    protected Context context;
    protected ContextInfo contextInfo;

    private GameObject animationInstance;

    public void createContext()
    {
        health = villain.health;
        ennemy = GameObject.FindGameObjectWithTag("Player");
        target = ennemy.transform;
        ennemyController = ennemy.GetComponent<RustyForestCharacter>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        nmRealTimeBuilder = GetComponent<NavMeshRealTimeBuilder>();
        navMeshAgent.speed = villain.speed;
        navMeshAgent.acceleration = villain.acceleration;

        contextInfo = new ContextInfo(
            null,
            villain,
            navMeshAgent,
            target,
            transform,
            nmRealTimeBuilder,
            this
            );
        context = new Context(contextInfo);
    }

    //default move method, maybe override
    //mainly called in chaseState
    public virtual void move(Vector3 position)
    {
        navMeshAgent.isStopped = false;
        navMeshAgent.SetDestination(position);
        transform.LookAt(position);
    }

    public void attack(ContextInfo infos)
    {
        if (ennemyController.health > 0)
        {
            concreteAttack(infos);
        }
    }

    public abstract void concreteAttack(ContextInfo infos);

    //This is the only method that change state outside of context
    public void retreat()
    {
        context.changeState(new EscapeState(context));
    }

    //maybe overrided
    public virtual void villainIsDead(GameObject villain)
    {

    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == SharedConst.TAG_PINECONE)
        {
            //@todo this should be in pinecone ScriptableObject ?
            health -= SharedConst.PINECONE_DAMMAGE;

            if (villain.dammageAnimation)
            {
                animationInstance = Instantiate(villain.dammageAnimation, transform.position, Quaternion.identity);
                // no sound is played if the villain die before the animation is spawned and it's parent is the dead villain
                // animationInstance.transform.parent = transform;
                StartCoroutine(WaitAndDisableAnimation(animationInstance));
            }

            if (health <= 0)
            {
                villainIsDead(gameObject);
                OnDeath(gameObject);
                gameObject.SetActive(false);
            }
        }
    }

    IEnumerator WaitAndDisableAnimation(GameObject animation)
    {
        yield return new WaitForSeconds(1f);
        animation.SetActive(false);
    }
}
