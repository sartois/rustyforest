﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Must;

public class VillainsManager : MonoBehaviour
{
    public Villain firstVillain;
    public Villain[] villains;
    public int maxBoids = 6; 
    public GameObject visualDebug = null;
    

    public bool showDebug = false;

    private const float startAngle = (float)(-Math.PI / 4);
    private const float startAngleReverse = (float)(-3* Math.PI / 4);

    private const int raycastHardLimit = 50;
    private const float raycastRadius = 10f;
    private Transform player;
    private float height = 30f;
    private Vector3 dummy;
    private RaycastHit hit;
    private Vector3 dir;
    private Vector3 approximateHit;
    private float incrementAngle;
    private List<Villain> allowedVillains = new List<Villain>();
    private int lastUserTile = 0;
    private int raysToShoot = 16;
    private float radius = raycastRadius;
    private int raycastCounter = 0;
    private List<GameObject> activeVillainsInstance = new List<GameObject>();
    private List<Villain> villainsCounter = new List<Villain>();
    private bool hasSpawnedFirstVillain = false;
    private float timeSpan = 0;
    private float periodUpdate = 2.0f;

    private void OnEnable()
    {
        AVillainController.OnDeath += HandleVillainDeath;
    }

    private void OnDisable()
    {
        AVillainController.OnDeath -= HandleVillainDeath;
    }

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        showDebug = showDebug && visualDebug != null;
        incrementAngle = (float)(Math.PI / 2 / raysToShoot);
    }

    void Update()
    {
        transform.position = stayAboveThePlayer();

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.V))
        {
            AddVillain(villains[0]);
        }
#endif

        timeSpan += Time.deltaTime;
        if (timeSpan > periodUpdate)
        {
            timeSpan = 0;
            villainSpawnerRoutine();
        }

        destroyLostVillains();
    }

    Vector3 stayAboveThePlayer()
    {
        dummy = player.position;
        dummy.y = height;
        return dummy;
    }

    void villainSpawnerRoutine()
    {
        if (shouldSpawnVillain()) {
            AddVillain(getNextVillain());
        }
    }

    //@todo Warning, only works for straight tile
    bool shouldSpawnVillain()
    {
        float proba = Utils.getCroissantDifficulty(UserIsOnTile.tileNb * 1f);

        //Do not spawn before some thresold
        if (transform.position.z <= SharedConst.START_SPAWNING_VILLAIN_AT)
        {
            return false;
        } else if (transform.position.z >= SharedConst.START_SPAWNING_VILLAIN_AT && transform.position.z <= (SharedConst.START_SPAWNING_VILLAIN_AT + 10f))
        {
            //no random for first villain, show it once then never
            return villainsCounter.Count < 1 && !hasSpawnedFirstVillain;
        } else
        {
            int hardLimitVillain = UserIsOnTile.tileNb <= 3 ? 2 : UserIsOnTile.tileNb <= 6 ? 3 : 4;
            if (villainsCounter.Count >= hardLimitVillain)
            {
                return false;
            }
            
            float chance = UnityEngine.Random.value;
            bool test = chance < proba;
            return test;
        }
    }

    Villain getNextVillain()
    {
        if (villains.Length == 0)
        {
            Debug.LogWarning("Villains list is empty, can't add new one");
            return null;
        }
        
        // if user has moved in a new tile, update the villains list, if not, use it as is
        if (UserIsOnTile.tileNb != lastUserTile)
        {
            lastUserTile = UserIsOnTile.tileNb;
            allowedVillains.Clear();

            for (int i = 0; i < villains.Length; i++)
            {
                if (villains[i].difficulty <= lastUserTile)
                {
                    allowedVillains.Add(villains[i]);
                }
            }
        }
        
        if (transform.position.z >= SharedConst.START_SPAWNING_VILLAIN_AT && transform.position.z <= (SharedConst.START_SPAWNING_VILLAIN_AT+ 10f))
        {
            hasSpawnedFirstVillain = true;
            return firstVillain;
        } else
        {
            return allowedVillains[UnityEngine.Random.Range(0, allowedVillains.Count)];
        }
    }

    void AddVillain(Villain toAdd)
    {
        if (toAdd.isBoids)
        {
            //There is only one boids at the same time, so count only once
            if (BoidsController.size() == 0) villainsCounter.Add(toAdd);

            int boidsLimit = BoidsController.size() <= maxBoids ? Mathf.Min(toAdd.boidsInstance, maxBoids - BoidsController.size()) : 0;
            for (int i = 0; i < boidsLimit; i++)
            {
                Vector3 position =  getNextVillainPosition();
                if (position != Vector3.zero)
                {
                    GameObject instance = Instantiate(toAdd.avatar, position, Quaternion.identity);
                    BoidsController.addBoid(instance);
                    activeVillainsInstance.Add(instance);
                }
            }
        } else
        {
            Vector3 position = getNextVillainPosition();
            if (position != Vector3.zero)
            {
                GameObject instance = Instantiate(toAdd.avatar, position, Quaternion.identity);
                activeVillainsInstance.Add(instance);
                villainsCounter.Add(toAdd);
            }
        }
    }

    Vector3 getNextVillainPosition()
    {
        //do not always put a villain at the same side
        bool reverse = UnityEngine.Random.value > 0.5;

        float angle = reverse ? startAngleReverse : startAngle;
        bool foundPlace = false;
        raycastCounter = 0;

        //launch raycasts around a 90° arc behind the player
        //increase radius until we find a correct poisition or we reach some limit
        do
        {
            for (int i = 0; i < raysToShoot; i++)
            {
                float x = transform.position.x + (Mathf.Cos(angle) * radius);
                float z = transform.position.z + (Mathf.Sin(angle) * radius);

                approximateHit = new Vector3(x, SharedConst.GROUND_ALTITUDE_WORLD, z);
                dir = (approximateHit - transform.position).normalized;

                if (showDebug)
                {
                    Debug.DrawLine(transform.position, approximateHit, Color.red, 30f);
                }

                raycastCounter++;

                if (Physics.Raycast(transform.position, dir, out hit))
                {
                    if (showDebug)
                    {
                        Debug.DrawRay(transform.position, dir, Color.yellow, 30f);
                    }

                    //@todo Using name is fragile. Can we use layers ?
                    if (hit.collider.name.StartsWith("Terrain"))
                    {
                        if (hit.point.y < SharedConst.ALTITUDE_ALLOWED_FOR_SPAWN_MAX && hit.point.y > SharedConst.ALTITUDE_ALLOWED_FOR_SPAWN_MIN)
                        {
                            return hit.point;
                        }
                    }
                }

                if (reverse)
                {
                    angle += incrementAngle;
                }
                else
                {
                    angle -= incrementAngle;
                }

                if (raycastCounter >= raycastHardLimit)
                {
                    return Vector3.zero;
                }
            }

            if (showDebug)
            {
                foundPlace = true;
            }

            //reset and increase radius if not found
            radius = (foundPlace) ? raycastRadius : radius + 1f;
            angle = startAngle;

        } while ((!foundPlace));

        return Vector3.zero;
    }

    void HandleVillainDeath(GameObject villain)
    {
        activeVillainsInstance.Remove(villain);

        Villain type = villain.GetComponent<AVillainController>().villain;

        if (!type.isBoids || type.isBoids && BoidsController.size() == 0)
        {
            for (int i = villainsCounter.Count - 1; i >= 0; i--)
            {
                if (villainsCounter[i] == type)
                {
                    //remove the first found villain type
                    villainsCounter.RemoveAt(i);
                    break;
                }
            }
        }

        foreach (GameObject aliveVillain in activeVillainsInstance)
        {
            aliveVillain.GetComponent<AVillainController>().retreat();
        }
    }

    //Sometimes they get lost. I think thats when Joddy go very fast and leave their navmesh too early
    void destroyLostVillains()
    {
        for (int i = activeVillainsInstance.Count - 1; i >= 0; i--)
        {
            GameObject go = activeVillainsInstance[i];
            if (Vector3.Distance(transform.position, go.transform.position) > SharedConst.LOST_THRESHOLD)
            {
                Villain type = go.GetComponent<AVillainController>().villain;

                if (!type.isBoids || type.isBoids && BoidsController.size() == 0)
                {
                    for (int j = villainsCounter.Count - 1; j >= 0; j--)
                    {
                        if (villainsCounter[j] == type)
                        {
                            //remove the first found villain type
                            villainsCounter.RemoveAt(j);
                            goto Found;
                        }
                    }

                    Found:
                        Debug.Log("Found");
                }
               
                activeVillainsInstance.RemoveAt(i);
                Destroy(go);
            }
        }
    }
}
