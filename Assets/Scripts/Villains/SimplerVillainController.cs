﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class  SimplerVillainController : AVillainController
{
    public void Start()
    {
        createContext();
    }

    public void Update()
    {
        context.Update();
    }

    //Try to touch the target
    public override void concreteAttack(ContextInfo infos)
    {
        navMeshAgent.SetDestination(infos.target.position);
        transform.LookAt(infos.target.position);
    }
}
