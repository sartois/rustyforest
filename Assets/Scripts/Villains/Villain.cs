﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Villain")]
public class Villain : ScriptableObject
{
    public GameObject avatar;
    public Ammunition ammunition = null;
    public GameObject animation;
    public GameObject dammageAnimation;
    //From which tile villain may appear
    public int difficulty = 2;
    public int health = 10;
    public float speed = 3f;
    public float acceleration = 2f;
    public float attackDistanceThreshold = 10.0f;
    //lower is faster. Timelapse between each context update call
    [Range(0,1)]
    public float agility = 0.3f;
    public float escapeMagnitude = 10.0f;
    public int escapeDuration = 1;
    public int birthAnimationLength = 1;
    public int directDammage = 1;
    public bool isBoids = false;
    public int boidsInstance = 3;
}

