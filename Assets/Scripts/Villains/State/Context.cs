﻿using UnityEngine;

public class Context
{
    private float periodUpdate;
    private AState currentState;
    private ContextInfo contextInfo;
    private float timeSpan = 0;

    public Context(ContextInfo infos)
    {
        contextInfo = infos;
        periodUpdate = infos.villain.agility;
    }

    public void Update()
    {
        if (currentState == null)
        {
            currentState = new BirthState(this);
            return;
        }

        timeSpan += Time.deltaTime;
        if (timeSpan > periodUpdate  || currentState.isNew)
        {
            timeSpan = 0;
            currentState.handle(contextInfo);
            currentState.isNew = false;
        }
    }

    public void changeState(AState nextState)
    {
        currentState = nextState;
    }
}
