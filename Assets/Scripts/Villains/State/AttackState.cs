﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : AState
{
    public AttackState(Context ctxt) : base(ctxt)
    { }

    public override void handle(ContextInfo infos)
    {
        infos.villainController.attack(infos);

        if (mustChase(infos))
        {
            context.changeState(new ChaseState(context));
        }
    }
}
