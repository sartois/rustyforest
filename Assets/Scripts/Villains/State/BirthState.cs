﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class BirthState : AState
{
    private bool isBorn = false;
    private bool enableAnimation = false;
    private ContextInfo infos;
    private GameObject animation;

    public BirthState(Context ctxt) : base(ctxt)
    { 
    }

    public override void handle(ContextInfo infos)
    {
        if (!isBorn) {
            isBorn = true;
            this.infos = infos;

            //Disabled. Needs more work but no time for now
            if (enableAnimation)
            {
                animation = AVillainController.Instantiate(infos.villain.animation, infos.villainT.position, Quaternion.identity);
                infos.villainController.StartCoroutine(WaitAndDisable());
                infos.villainController.StartCoroutine(MoveToPosition());
            }

            infos.villainT.LookAt(infos.target);

            if (!AVillainController.freeze) { 
                infos.villainController.StartCoroutine(ComeToLife());
            }
        }
    }

    /**
     * The villain rises from the ground
     * Can't make it work with realtime navmesh 
     */
    private IEnumerator MoveToPosition()
    {
        var currentPos = infos.villainT.position;
        Vector3 target = infos.villainT.position;
        target.y += 3;

        float timeToMove = 1f;
        var t = 0f;

        while (t < 1)
        {
            t += Time.deltaTime / timeToMove;
            infos.villainT.position = Vector3.Lerp(currentPos, target, t);
            //infos.villainT.LookAt(infos.target);
            yield return null;
            //AVillainController.Destroy(animation);
        }
    }
    private IEnumerator WaitAndDisable()
    {
        yield return new WaitForSeconds(infos.villain.birthAnimationLength);
        AVillainController.Destroy(animation);
    }

    private IEnumerator ComeToLife()
    {
        yield return new WaitForSeconds(infos.villain.birthAnimationLength);
        context.changeState(new ChaseState(context));
    }

}
