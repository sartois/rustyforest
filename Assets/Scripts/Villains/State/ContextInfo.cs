﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ContextInfo
{
    public Animator animator;
    public Villain villain;
    public NavMeshAgent navMeshAgent;
    public Transform target;
    public Transform villainT;
    public NavMeshRealTimeBuilder navMeshRealTimeBuilder;
    public AVillainController villainController;

    public ContextInfo(
        Animator animator, 
        Villain villain, 
        NavMeshAgent navMeshAgent, 
        Transform target, 
        Transform villainT, 
        NavMeshRealTimeBuilder navMeshRealTimeBuilder, 
        AVillainController villainController)
    {
        this.animator = animator;
        this.villain = villain;
        this.navMeshAgent = navMeshAgent;
        this.target = target;
        this.villainT = villainT;
        this.navMeshRealTimeBuilder = navMeshRealTimeBuilder;
        this.villainController = villainController;
    }
}
