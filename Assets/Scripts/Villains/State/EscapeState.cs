﻿using System.Collections;
using UnityEngine;

//Escape is the only state not triggered by another state
//But an event when villain is hit
public class EscapeState : AState
{
    public EscapeState(Context ctxt) : base(ctxt)
    { }

    public override void handle(ContextInfo infos)
    {
        Vector3 dirToTarget = (infos.target.position - infos.villainT.position);

        if (isNew) {
            if (dirToTarget.magnitude < SharedConst.DONT_ESCAPE_THRESHOLD) { 
                Vector3 escapeDir = (infos.villainT.position - infos.target.position).normalized * infos.villain.escapeMagnitude;
                Vector3 escapePos = infos.villainT.position + escapeDir;

                infos.villainController.move(escapePos);
                infos.villainController.StartCoroutine(WaitAndChangeState(infos.villain.escapeDuration));
            } else
            {
                context.changeState(new ChaseState(context));
            }
        }
    }

    private IEnumerator WaitAndChangeState(int escapeDuration)
    {
        yield return new WaitForSeconds(escapeDuration);
        context.changeState(new ChaseState(context));
    }
}
