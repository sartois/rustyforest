﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AState
{
    protected Context context;
    //the state is new, don't wait for update cycle to handle
    public bool isNew = true;

    //@todo use delegate to only share Context.changeState
    public AState(Context ctxt)
    {
        context = ctxt;
    }

    public abstract void handle(ContextInfo infos);

    public bool mustChase(ContextInfo infos)
    {
        float distance = Vector3.Distance(infos.villainT.position, infos.target.position);
        return distance >= infos.villain.attackDistanceThreshold;
    }
}
