﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseState : AState
{
    public ChaseState(Context ctxt) : base(ctxt)
    { }

    public override void handle(ContextInfo infos)
    {
        infos.villainController.move(infos.target.position);

        if (Vector3.Distance(infos.villainT.position, infos.target.position) <= infos.villain.attackDistanceThreshold)
        {
            context.changeState(new AttackState(context));
        }
    }
}
