﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowingVillainController : AVillainController
{
    public float timeBetweenShots = 0.5f;
    public float ammunitionStartSpeed = 100;
    private float offsetForwardShoot = 2;
    private float timeShoot = 0;

    public void Start()
    {
        createContext();
    }

    public void Update()
    {
        context.Update();
    }

    public override void concreteAttack(ContextInfo infos)
    {
        navMeshAgent.SetDestination(transform.position);

        timeShoot -= Time.deltaTime;

        if (timeShoot <= 0)
        {
            timeShoot = timeBetweenShots;
            
            Transform proj = GameObject.Instantiate<Transform>(villain.ammunition.avatar.transform,
                    transform.position + (transform.forward * offsetForwardShoot) + (transform.up *0.3f), transform.rotation);

            //proj.transform.parent = transform;

            proj.GetComponentInChildren<Rigidbody>().AddForce(transform.forward * ammunitionStartSpeed, ForceMode.Impulse);

            StartCoroutine(WaitAndDisableAmunition(proj.gameObject));
        }
    }

    IEnumerator WaitAndDisableAmunition(GameObject amunition)
    {
        yield return new WaitForSeconds(30f);
        amunition.SetActive(false);
    }
}
