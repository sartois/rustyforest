﻿using System.Collections.Generic;
using UnityEngine;

public class BoidsController : SimplerVillainController
{
    static List<GameObject> boids = new List<GameObject>();

    static public void addBoid(GameObject boid)
    {
        boids.Add(boid);
    }
    static public int size()
    {
        return boids.Count;
    }

    public Vector3 getDirection(Vector3 destination)
    {
        return (destination - transform.position).normalized;
    }

    public override void villainIsDead(GameObject villain)
    {
        boids.Remove(villain);
    }

    //Our first try was to implement the boids algo
    //But it's very hard with a navmeshagent to have something look good
    //So we simply define a leader, and they following it
    //having the same lookat create alignment, and they are kept separate by the navmesh motor
    public override void move(Vector3 destination)
    {
        int index = boids.IndexOf(gameObject);
        Vector3 realDest = destination;

        if (index != 0)
        {
            GameObject leader = boids[0];
            realDest = leader.transform.position;
        } else if (boids.Count > 1)
        {
            float distAverage = 0;
            Vector3 averagePosition = Vector3.zero;

            //get average dist with crowds
            foreach(GameObject boid in boids)
            {
                if (boid != gameObject)
                {
                    distAverage += Vector3.Distance(transform.position, boid.transform.position);
                    averagePosition += boid.transform.position;
                }
            }

            distAverage /= boids.Count - 1;
            averagePosition /= boids.Count - 1;

            if (distAverage > 5)
            {
                realDest = averagePosition;
            }
        }

        navMeshAgent.isStopped = false;
        navMeshAgent.SetDestination(realDest);
        transform.LookAt(destination);
    }


    public Vector3 getAverageDir(Vector3 destination)
    {
        Vector3 averageDir = Vector3.zero;

        foreach (GameObject boid in boids)
        {
            averageDir += boid.GetComponent<BoidsController>().getDirection(destination);
        }

        averageDir /= boids.Count;

        return averageDir;
    }

    public Vector3 getCohesion()
    {
        Vector3 sum = Vector3.zero;
        int count = 0;

        foreach (GameObject boid in boids)
        {
            float d = Vector3.Distance(transform.position, boid.transform.position);
            if (d>0f)
            {
                sum += boid.transform.position;
                count++;
            }
        }

        if (count > 0)
        {
            sum /= count;
            return sum.normalized;
        }
      
        return Vector3.zero;
    }
}
