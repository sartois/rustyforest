﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeVillainController : AVillainController
{
    void Start()
    {
        createContext();
    }

    void Update()
    {
        context.Update();
    }

    /**
     * Some kind of pseudo attack, stay behing the player
     */
    public override void concreteAttack(ContextInfo infos)
    {
        Vector3 dirToTarget = (infos.target.position - transform.position);
        Vector3 destination = (dirToTarget).normalized * (dirToTarget.magnitude * 0.5f);

        if (dirToTarget.magnitude > 2)
        {
            destination = transform.position + destination;
        } else
        {
            destination = transform.position;
        }

        navMeshAgent.SetDestination(destination);
        transform.LookAt(infos.target.position);
    }
}

