﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Adapted from https://github.com/mirrorfishmedia/EndlessRunnerUnity/blob/master/ColorRun/Assets/Scripts/LevelLayoutGenerator.cs
 * By  Matt MirrorFish, @see https://www.youtube.com/channel/UCxRW28Si8_Vb27-IDhFMMKQ
 */
public class TilesInterlockController : MonoBehaviour
{
    static public int tilesAdded { get; private set; } = 0;
    
    public Tile[] tiles;
    public Tile firstTile;
    public int tilesToAdd = 2;
    public Vector3 spawnOrigin;

    private Vector3 spawnPosition;
    private Tile previousTile;
    private Dictionary<int, GameObject> activeTileInstances = new Dictionary<int, GameObject>();

    private void OnEnable()
    {
        ExitWatcher.OnTileExited += AddTile;
        ExitWatcher.OnTileExited += RemoveTile;
    }

    private void OnDisable()
    {
        ExitWatcher.OnTileExited -= AddTile;
        ExitWatcher.OnTileExited -= RemoveTile;
    }


    void Start()
    {
        for(int i = 0; i < tilesToAdd; i++)
        {
            AddTile();
        }
    }

  
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            AddTile();
        }
    }

    void AddTile()
    {
        Tile nextTile = tilesAdded == 0 ? firstTile : PickNextTile();

        Vector3 nextPos = spawnPosition + spawnOrigin;
        //Keep player at 0
        nextPos.y = -SharedConst.GROUND_ALTITUDE_RELATIVE;

        GameObject tileTerrain = nextTile.tileTerrain;
        previousTile = nextTile;
        
        GameObject tileInstance = Instantiate(tileTerrain, nextPos, Quaternion.identity);
        activeTileInstances.Add(tilesAdded, tileInstance);

        tilesAdded++;
    }

    void RemoveTile()
    {
        if(UserIsOnTile.tileNb >= 4)
        {
            int tileToRemoveIndex = UserIsOnTile.tileNb - 3;
            GameObject tileInstanceToRemove;
            if (!activeTileInstances.TryGetValue(tileToRemoveIndex, out tileInstanceToRemove))
            {
                Debug.LogWarning("Can't find tile with key " + tileToRemoveIndex + ", very strange. User is on tile " + UserIsOnTile.tileNb);
                return;
            }

            //Try to not add and remove in the same frame
            StartCoroutine(CleanAndMove(tileToRemoveIndex, tileInstanceToRemove));
        }
    }

    Tile PickNextTile()
    {
        List<Tile> consistentTiles = new List<Tile>();
        Tile nextTile = null;
        Tile.Direction nextRequiredDirection;

        switch (previousTile.exitDirection)
        {
            case Tile.Direction.North:
                nextRequiredDirection = Tile.Direction.South;
                spawnPosition = spawnPosition + new Vector3(0f, 0, previousTile.size.y);

                break;
            case Tile.Direction.East:
                nextRequiredDirection = Tile.Direction.West;
                spawnPosition = spawnPosition + new Vector3(previousTile.size.x, 0, 0);
                break;
            
            case Tile.Direction.West:
                nextRequiredDirection = Tile.Direction.East;
                spawnPosition = spawnPosition + new Vector3(-previousTile.size.x, 0, 0);
                break;

            case Tile.Direction.South:
            default:
                throw new Exception("Bad Tile format. Must not exit south : avoid handling tiles overlap");   
        }

        for (int i = 0; i < tiles.Length; i++)
        {
            if (tiles[i].entryDirection == nextRequiredDirection)
            {
                consistentTiles.Add(tiles[i]);
            }
        }

        int index = UnityEngine.Random.Range(0, consistentTiles.Count);
        nextTile = consistentTiles[index];

        return nextTile;
    }

    IEnumerator CleanAndMove(int tileToRemoveIndex, GameObject tileInstanceToRemove)
    {
        yield return new WaitForSeconds(2f);

        activeTileInstances.Remove(tileToRemoveIndex);
        Destroy(tileInstanceToRemove);

        //move the first tile
        GameObject firstTile = activeTileInstances[0];
        firstTile.transform.Translate(0, 0, SharedConst.TILE_DEPTH);
    }
}
