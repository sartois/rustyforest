﻿using System.Collections;
using UnityEngine;

public class ExitWatcher : MonoBehaviour
{
    public delegate void ExitHandler();
    public static event ExitHandler OnTileExited;

    private bool exited = false;

    public float delay = 5f;

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (!exited)
            {
                exited = true;
                OnTileExited();
                StartCoroutine(WaitAndDeactivate());
            }
        } else
        {
            Debug.Log(other);
        }
    }

    IEnumerator WaitAndDeactivate()
    {
        yield return new WaitForSeconds(delay);
        //@todo disable far away tiles
    }
}
