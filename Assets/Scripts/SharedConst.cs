﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SharedConst
{
    public const float GROUND_ALTITUDE_RELATIVE = 5.25f;
    public const float GROUND_ALTITUDE_WORLD = 0f;

    public const float ALTITUDE_ALLOWED_FOR_SPAWN_MIN = -0.2f;
    public const float ALTITUDE_ALLOWED_FOR_SPAWN_MAX = 0.2f;

    public const string TAG_VILLAIN = "Villain";
    public const string TAG_AMMUNITION = "Ammunition";
    public const string TAG_PINECONE = "Pinecone";

    public const int PINECONE_DAMMAGE = 10;

    public const float START_SPAWNING_VILLAIN_AT = 40f;

    public const float TILE_DEPTH = 100f;

    public const float DONT_ESCAPE_THRESHOLD = 25f;
    public const float LOST_THRESHOLD = 75f;
}
